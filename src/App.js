import React, {useEffect} from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './Components/Header/Header';
import Home from './Components/Home/Home';
import Checkout from './Components/Checkout/Checkout';
import Login from './Components/Login/Login';
import { useStateValue } from './Services/StateProvider';
import { auth } from './Firebase';

function App() {
  const [{basket},dispatch] = useStateValue();

  useEffect(() => {
    const unsubscribe =  auth.onAuthStateChanged((authUser)=>{
     if(authUser){
       dispatch({
         type:"SET_USER",
         user:authUser
       })
     }
     else{
       //user is logged out

       dispatch({
        type:"SET_USER",
        user:null
      })
     }
   })
    return () => {
      unsubscribe();
    }
  }, [])

  return (  
    < Router >
      <div className="App">
        <Switch>
          <Route path="/checkout">
            <Header></Header>
           <Checkout></Checkout>
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/">
            <Header></Header>
            <Home></Home>
          </Route>
        </Switch>
      </div >
    </Router >  
  );  
  
}

export default App;
