import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyDTSQxoemx7ci_bmEGkQbT1x75pJ-zAI9w",
    authDomain: "clone-f30b8.firebaseapp.com",
    databaseURL: "https://clone-f30b8.firebaseio.com",
    projectId: "clone-f30b8",
    storageBucket: "clone-f30b8.appspot.com",
    messagingSenderId: "176796449794",
    appId: "1:176796449794:web:53d6026d4e96318a857b1b",
    measurementId: "G-T8SP4NBV1F"
});

const db = firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };