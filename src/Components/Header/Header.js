import React from 'react'
import './Header.css';
import { Link } from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import { useStateValue} from '../../Services/StateProvider';


function Header() {
    const [{ basket , user}, dispatch] = useStateValue();
    console.log(basket);
    console.log(user);
    return (
        <nav className="header">
            {/* logo on left */}
            <Link to="/">
                <img className="header__logo" src="http://pngimg.com/uploads/amazon/amazon_PNG11.png" alt="amazon logo"></img>
            </Link>
            
            {/* search bar */}
            <div className="header__search">
                <input type="text" className="header__searchInput"></input>
                <SearchIcon className="header__searchIcon"></SearchIcon>
            </div>

            {/* buttons on right */}
            <div className="header__nav">
                {/* 1st link */}
                <Link to="/login" className="header__link">
                    <div className="header__option">
    <span className="header__optionLineOne">Hello {user?.email}</span>
                        <span className="header__optionLineTwo">Sign In</span>
                    </div>                   
                </Link>
                {/* 2nd link */}
                <Link to="/" className="header__link">
                    <div className="header__option">
                        <span className="header__optionLineOne" >Returns</span>
                        <span className="header__optionLineTwo">& Orders</span>
                    </div>                   
                </Link>
                {/* 3rd link */}
                <Link to="/" className="header__link">
                    <div className="header__option">
                        <span className="header__optionLineOne">Your</span>
                        <span className="header__optionLineTwo">Prime</span>
                    </div>                   
                </Link>

            </div>
            {/* Basket icon */}
            <Link to="/checkout" className="header__link">
                <div className="header__basketIcon">
                    {/* shopping basket icon */}
                    <ShoppingBasketIcon></ShoppingBasketIcon>
                    {/* Number of items in basket */}
                    <span className="header__optionLineTwo header_basketcount">{basket?.length}</span>
                </div>
            </Link>
        </nav>
    )
}

export default Header;
