import React from 'react'
import './Home.css';
import Product from '../Product/Product';

function Home() {
    return (
        <div className="home">            
            <img className="home__image" alt="" src="https://m.media-amazon.com/images/G/31/AmazonVideo/2019/1102620_MLP_1440x675_apv189_V3._SY1200_FMJPG_.jpg"></img>

        <div className="home__row">
            <Product
                key = {1}
                id="123421"
                title="Reach Contempo Foldable Exercise Cycle for Home Gym | Exercise Bike with Back Support Seat and Resistance Rope"
                price={11.30}
                rating={3}
                image="https://m.media-amazon.com/images/I/51kDplcA7AL._AC_SL260_.jpg"
            />
            <Product
                key = {2}
                id="123421"
                title="Reach Contempo Foldable Exercise Cycle for Home Gym | Exercise Bike with Back Support Seat and Resistance Rope"
                price={11.30}
                rating={3}
                image="https://m.media-amazon.com/images/I/51kDplcA7AL._AC_SL260_.jpg"
            />
        </div>

        
        <div className="home__row">
            <Product
                key = {3}
                id="123421"
                title="Reach Contempo Foldable Exercise Cycle for Home Gym | Exercise Bike with Back Support Seat and Resistance Rope"
                price={11.30}
                rating={3}
                image="https://m.media-amazon.com/images/I/51kDplcA7AL._AC_SL260_.jpg"
            />
            <Product
                key = {4}
                id="123421"
                title="Reach Contempo Foldable Exercise Cycle for Home Gym | Exercise Bike with Back Support Seat and Resistance Rope"
                price={11.30}
                rating={3}
                image="https://m.media-amazon.com/images/I/51kDplcA7AL._AC_SL260_.jpg"
            />
              <Product
                key = {5}
                id="123421"
                title="Reach Contempo Foldable Exercise Cycle for Home Gym | Exercise Bike with Back Support Seat and Resistance Rope"
                price={11.30}
                rating={3}
                image="https://m.media-amazon.com/images/I/51kDplcA7AL._AC_SL260_.jpg"
            />
        </div>
            
        </div>
    )
}

export default Home;
