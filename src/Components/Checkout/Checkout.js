import React from 'react';
import './Checkout.css';
import { useStateValue } from '../../Services/StateProvider';
import CheckoutProduct from '../CheckoutProduct/CheckoutProduct';
import Subtotal from '../Subtotal/Subtotal';

function Checkout() {
    const [{basket}] = useStateValue();
    return (
        <div className="checkout">
            <div className="checkout__left">
                <img className="checkout_ad"
                    src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg" alt=""></img>
                {basket?.length === 0 ? (
                    <div>
                        <h2>Your Shopping Basket is empty</h2>
                        <p>You have no items in your basket.</p>
                    </div>
                ) : (
                        <div>
                            <h2 className="checkout_title">Your shopping Basket</h2>
                            {
                                basket?.map(item => (
                                    <CheckoutProduct
                                        id={item.id}
                                        title={item.title}
                                        image={item.image}
                                        rating={item.rating}
                                        price={item.price}
                                    />
                                ))
                            }
                        </div>
                    )}
            </div>
            {basket?.length > 0 && (
                <div className="checkout__right">
                    <Subtotal />
                </div>
            )}

        </div>
    );
}

export default Checkout;
