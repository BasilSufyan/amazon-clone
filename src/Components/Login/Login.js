import React, { useState } from 'react';
import './Login.css';
import { Link, useHistory } from 'react-router-dom';
import {auth} from '../../Firebase';

function Login() {
    const history = useHistory();

    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');

    const signIn = event =>{
        event.preventDefault();

        auth.signInWithEmailAndPassword(email,password)
        .then(auth =>{
            // logged in sucessfull. redirect
            console.log(auth);
            history.push("/");
        })
        .catch(e =>{
            alert(e.message);
        });
    }

    const register = event =>{
        event.preventDefault();
        auth.createUserWithEmailAndPassword(email,password)
        .then( auth=>{
            // user registered. login
            console.log(auth);
            history.push("/");
        })
        .catch(e =>{
            alert(e.message);
        });
    }
    return (
        <div className="login">
            <Link to="/">
                <img className="login__logo" alt=""
                src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Amazon_logo.svg/1024px-Amazon_logo.svg.png"></img>
            </Link>
            <div className="login__container">
                 <h1>Sign In</h1>   
                 <form>
                     <h5>E-mail</h5>
                     <input value={email} onChange={event => setEmail(event.target.value)} type="text"></input>
                     <h5>Password</h5>
                     <input value={password} onChange={event => setPassword(event.target.value)} type="password"></input>
                     <button type="submit" className="login__submitbutton" onClick={signIn}>Sign In</button>
                 </form>
                 <p>
                    By continuing, you agree to Amazon's Conditions of Use and Privacy Notice.
                 </p>
                 <button className="login__registerbutton" onClick={register}>Create new Amazon Account</button>
            </div>
          

        </div>
    )
}

export default Login
